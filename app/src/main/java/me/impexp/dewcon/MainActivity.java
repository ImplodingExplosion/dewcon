/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.regex.Pattern;

import me.impexp.dewcon.filter.ASCIIFilter;
import me.impexp.dewcon.history.DewHistoryAdapter;
import me.impexp.dewcon.history.DewHistoryItem;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_SERVER = 1;
    private static final int REQUEST_SETTINGS = 2;
    private static final Pattern PATTERN_IP_ADDRESS =
            Pattern.compile("^(?:(?:25[0-5]|2[0-4]\\d|(?:[0-1]?\\d)?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|(?:[0-1]?\\d)?\\d)$");
    private static final Pattern PATTERN_FQDN =
            Pattern.compile("^(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?(?:\\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|-){0,61}[0-9A-Za-z])?)*\\.?$");
    private SharedPreferences prefs;
    private Socket socket;
    private ListView historyLV;
    private DewHistoryAdapter historyAdapter;
    private int historyPos = -1;
    private EditText commandET;
    private Button sendB;
    private MenuItem connectMI;
    private String rememberedServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_USER);
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        rememberedServer = prefs.getString(getString(R.string.pref_server), "");

        historyLV = (ListView) findViewById(R.id.historyLV);
        commandET = (EditText) findViewById(R.id.commandET);
        sendB = (Button) findViewById(R.id.sendB);
        sendB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCommand();
            }
        });
        updateSendButtonVisibility();
        commandET.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        sendCommand();
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        updateCommand(getPreviousCommand(historyPos));
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                        updateCommand(getNextCommand(historyPos));
                        return true;
                    }
                }
                return false;
            }
        });
        commandET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendCommand();
                    return true;
                }
                return false;
            }
        });
        ArrayList<InputFilter> filters = new ArrayList<>(Arrays.asList(commandET.getFilters()));
        filters.add(new ASCIIFilter());
        commandET.setFilters(filters.toArray(new InputFilter[filters.size()]));

        historyLV.setAdapter(historyAdapter = new DewHistoryAdapter(this));

        if (prefs.getBoolean(getString(R.string.pref_auto_connect), false))
            connect(rememberedServer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        connectMI = menu.findItem(R.id.action_connect);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (connectMI == null) connectMI = menu.findItem(R.id.action_connect);
        if (socket != null) connectMI.setTitle(R.string.action_disconnect);
        else connectMI.setTitle(R.string.action_connect);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_connect:
                if (socket != null) disconnect();
                else connect();
                return true;
            case R.id.action_server_browser:
                startActivityForResult(new Intent(this, ServerBrowserActivity.class), REQUEST_SERVER);
                return true;
            case R.id.action_settings:
                startActivityForResult(new Intent(this, SettingsActivity.class), REQUEST_SETTINGS);
                return true;
            case R.id.action_about:
                showAboutDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SERVER) {
            if (resultCode == RESULT_OK) {
                String server = data.getStringExtra(ServerBrowserActivity.EXTRA_SERVER);
                sendCommand("connect " + server);
            }
        } else if (requestCode == REQUEST_SETTINGS) {
            historyLV.invalidateViews();
            updateSendButtonVisibility();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (socket != null) try {
            socket.close();
        } catch (IOException ignored) {}
    }

    private void updateSendButtonVisibility() {
        boolean show = prefs.getBoolean(getString(R.string.pref_show_send), true);
        sendB.setVisibility((show) ? View.VISIBLE : View.GONE);
        RelativeLayout.LayoutParams comLP = (RelativeLayout.LayoutParams) commandET.getLayoutParams();
        comLP.addRule(RelativeLayout.LEFT_OF, (show) ? R.id.sendB : 0);
        comLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, (show) ? 0 : RelativeLayout.TRUE);
    }

    private void connect() {
        final MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title(R.string.action_connect)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input("Host:Port", rememberedServer, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog materialDialog, CharSequence cs) {
                        connect(cs.toString());
                    }
                })
                .positiveText(R.string.action_connect)
                .negativeText(R.string.action_cancel)
                .build();
        if (dialog.getInputEditText() != null) {
            dialog.getInputEditText().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                        dialog.getActionButton(DialogAction.POSITIVE).performClick();
                        return true;
                    }
                    return false;
                }
            });
        }
        dialog.show();
    }

    private void connect(String server) {
        if (server == null || server.trim().length() == 0) return;
        try {
            String[] parts = server.trim().split("\\s*:\\s*", 2);
            int port = 2448;
            if (parts.length > 0 && parts.length < 3
                    && (PATTERN_IP_ADDRESS.matcher(parts[0]).matches()
                        || PATTERN_FQDN.matcher(parts[0]).matches())
                    && (parts.length == 1 || (port = Integer.parseInt(parts[1])) > 0 && port < 65536)) {
                String address = parts[0];
                rememberedServer = address + ":" + port;
                if (prefs.getBoolean(getString(R.string.pref_remember_server), false)) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(getString(R.string.pref_server), rememberedServer);
                    editor.commit();
                }
                connect(address, port);
            } else throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            toast("Invalid host/address and/or port", Toast.LENGTH_SHORT);
        }
    }

    private void connect(final String address, final int port) {
        if (socket != null && socket.isConnected()) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                InputStream in;
                try {
                    socket = new Socket();
                    socket.setKeepAlive(true);
                    socket.setSoTimeout(5000);
                    socket.connect(new InetSocketAddress(address, port), 3000);
                    in = socket.getInputStream();
                } catch (IOException e) {
                    socket = null;
                    toast("Unable to connect", Toast.LENGTH_SHORT);
                    return;
                }
                toast("Connected", Toast.LENGTH_SHORT);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sendB.setEnabled(true);
                    }
                });
                byte[] buffer = new byte[1024];
                while (socket != null && socket.isConnected()) {
                    try {
                        if (in.available() > 0) {
                            StringBuilder sb = new StringBuilder();
                            while (in.available() > 0) {
                                int read = in.read(buffer);
                                if (read > 0)
                                    sb.append(new String(buffer, 0, read, "US-ASCII"));
                            }
                            if (sb.length() > 0)
                                log(sb.toString().trim(), DewHistoryItem.DIRECTION_IN);
                        }
                    } catch (IOException ignored) {}
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException ignored) {}
                }
                socket = null;
                toast("Disconnected", Toast.LENGTH_SHORT);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sendB.setEnabled(false);
                    }
                });
            }
        }).start();
    }

    private void disconnect() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException ignored) {}
            socket = null;
            if (connectMI != null) connectMI.setTitle(R.string.action_connect);
            sendB.setEnabled(false);
        }
    }

    private void log(final String text, final int direction) {
        final Date timestamp = new Date();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                historyAdapter.add(timestamp, direction, text);
                historyAdapter.notifyDataSetChanged();
            }
        });
    }

    private int getPreviousCommand(int from) {
        if (from > historyAdapter.getCount()) from = historyAdapter.getCount();
        for (int i = from - 1; i >= 0; --i)
            if (historyAdapter.getItem(i).getDirection() == DewHistoryItem.DIRECTION_OUT)
                return i;
        return -1;
    }

    private int getNextCommand(int from) {
        if (from < -1) from = -1;
        for (int i = from + 1; i < historyAdapter.getCount(); ++i)
            if (historyAdapter.getItem(i).getDirection() == DewHistoryItem.DIRECTION_OUT)
                return i;
        return -1;
    }

    private void updateCommand(int pos) {
        if (pos > -1 && pos < historyAdapter.getCount()) {
            historyPos = pos;
            String cmd = historyAdapter.getItem(pos).getText();
            commandET.setText(cmd);
            commandET.setSelection(cmd.length());
        }
    }

    private void sendCommand() {
        if (sendCommand(commandET.getText().toString()))
            commandET.setText("");
    }

    private boolean sendCommand(String command, boolean log) {
        if (socket != null && socket.isConnected()) {
            if (log) {
                log(command, DewHistoryItem.DIRECTION_OUT);
                historyPos = historyAdapter.getCount();
            }
            try {
                socket.getOutputStream().write(command.getBytes("US-ASCII"));
            } catch (IOException e) {
                toast("An error occurred while attempting to send the command", Toast.LENGTH_SHORT);
            }
            return true;
        }
        return false;
    }

    private boolean sendCommand(String command) {
        return sendCommand(command, true);
    }

    private void toast(final CharSequence text, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, text, duration).show();
            }
        });
    }

    private void showAboutDialog() {
        new MaterialDialog.Builder(this)
                .title("About DewCON")
                .content(Html.fromHtml(getString(R.string.about)))
                .positiveText("OK")
                .show();
    }
}
