/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon.tool;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class JSON {
    public static JSONObject getObject(String url) {
        BufferedReader in = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.setConnectTimeout(3000);
            conn.setReadTimeout(5000);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null)
                sb.append(line).append('\n');
            return new JSONObject(sb.toString());
        } catch (IOException | JSONException e) {
            return null;
        } finally {
            if (in != null) try {
                in.close();
            } catch (IOException ignored) {}
        }
    }

    public static void getObjectAsync(final String url, final ObjectCallback callback) {
        if (url == null || callback == null) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                callback.done(getObject(url));
            }
        }).start();
    }

    public interface ObjectCallback {
        void done(JSONObject result);
    }
}
