/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon.filter;

import android.text.InputFilter;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;

public class ASCIIFilter implements InputFilter {
    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        if (source instanceof SpannableStringBuilder) {
            SpannableStringBuilder ssb = (SpannableStringBuilder) source;
            for (int i = end - 1; i >= start; --i)
                if (source.charAt(i) > 127)
                    ssb.delete(i, i + 1);
            return source;
        } else {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int i = start; i < end; ++i)
                if (source.charAt(i) < 128)
                    sb.append(source.charAt(i));
                else
                    keepOriginal = false;
            if (keepOriginal) {
                return null;
            } else if (source instanceof Spanned) {
                SpannableString ss = new SpannableString(sb);
                TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, ss, 0);
                return ss;
            } else {
                return sb;
            }
        }
    }
}
