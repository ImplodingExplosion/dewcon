/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon.history;

import java.util.Date;

public class DewHistoryItem {
    public static final int DIRECTION_OUT = 0;
    public static final int DIRECTION_IN = 1;
    private final Date timestamp;
    private final int direction;
    private final String text;

    public DewHistoryItem(Date timestamp, int direction, String text) {
        this.timestamp = timestamp;
        this.direction = direction;
        this.text = text;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getDirection() {
        return direction;
    }

    public String getText() {
        return text;
    }
}
