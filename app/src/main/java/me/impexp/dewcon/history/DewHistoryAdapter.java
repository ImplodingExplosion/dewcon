/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon.history;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.impexp.dewcon.R;

public class DewHistoryAdapter extends ArrayAdapter<DewHistoryItem> {
    private static final DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SS", Locale.US);
    private LayoutInflater inflater;
    private SharedPreferences prefs;

    public DewHistoryAdapter(Context context) {
        super(context, 0);
        inflater = LayoutInflater.from(context);
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.history_item, parent, false);
        }
        DewHistoryItem i = getItem(position);
        if (i != null) {
            TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
            if (timestamp != null) {
                timestamp.setText(dateFormat.format(i.getTimestamp()));
                boolean show = prefs.getBoolean(getContext().getString(R.string.pref_show_timestamps), true);
                timestamp.setVisibility((show) ? View.VISIBLE : View.GONE);
            }
            TextView direction = (TextView) convertView.findViewById(R.id.direction);
            if (direction != null) direction.setText((i.getDirection() == DewHistoryItem.DIRECTION_IN) ? ">" : "<");
            TextView text = (TextView) convertView.findViewById(R.id.text);
            if (text != null) text.setText(i.getText());
        }
        return convertView;
    }

    public void add(Date timestamp, int direction, String text) {
        add(new DewHistoryItem(timestamp, direction, text));
    }
}
