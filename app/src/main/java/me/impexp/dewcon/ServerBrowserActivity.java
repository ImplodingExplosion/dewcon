/*
 * Copyright 2016 David Vick <ImplodingExplosion>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.impexp.dewcon;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import me.impexp.dewcon.tool.JSON;

public class ServerBrowserActivity extends AppCompatActivity {
    public static final String EXTRA_SERVER = "me.impexp.dewcon.ServerBrowserActivity.EXTRA_SERVER";
    private static final String[] MASTER_SERVERS = new String[] { //TODO fetch from GitHub
            "http://158.69.166.144:8080/list",
            "http://eldewrito.red-m.net/list"
    };
    private int currentMS = 0;
    private int playersCount = 0, serverCount = 0;
    private TableLayout serversTL;
    private TextView toolbarCenterTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

        setContentView(R.layout.activity_server_browser);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        serversTL = (TableLayout) findViewById(R.id.serversTL);
        toolbarCenterTV = (TextView) findViewById(R.id.toolbarCenterTV);
        addHeader();
        updateServerList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.server_browser, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                updateServerList();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addHeader() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = LayoutInflater.from(ServerBrowserActivity.this);
                TableRow header = (TableRow) inflater.inflate(R.layout.server_row, serversTL, false);
                TextView nameTV = (TextView) header.findViewById(R.id.nameTV);
                nameTV.setText(R.string.server);
                nameTV.setTypeface(null, Typeface.BOLD);
                TextView gameTypeTV = (TextView) header.findViewById(R.id.gameTypeTV);
                gameTypeTV.setText(R.string.game_type);
                gameTypeTV.setTypeface(null, Typeface.BOLD);
                TextView mapTV = (TextView) header.findViewById(R.id.mapTV);
                mapTV.setText(R.string.map);
                mapTV.setTypeface(null, Typeface.BOLD);
                TextView playerCountTV = (TextView) header.findViewById(R.id.playerCountTV);
                playerCountTV.setText(R.string.players);
                playerCountTV.setTypeface(null, Typeface.BOLD);
                header.setClickable(false);
                serversTL.addView(header, 0);
                serversTL.addView(inflater.inflate(R.layout.row_separator, serversTL, false), 1);
            }
        });
    }

    private int startMS = 0;

    private void emptyServerList() {
        playersCount = 0;
        serverCount = 0;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbarCenterTV.setText("");
                serversTL.removeViews(2, serversTL.getChildCount() - 2);
            }
        });
    }

    private void returnServer(String address) {
        Intent data = new Intent();
        data.putExtra(EXTRA_SERVER, address);
        setResult(RESULT_OK, data);
        finish();
    }

    private void returnServer(String address, String password) {
        returnServer(address + " " + password);
    }

    private void addServer(final String address, final JSONObject server) {
        if (address == null || server == null) return;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflater = LayoutInflater.from(ServerBrowserActivity.this);

                final TableRow row = (TableRow) inflater.inflate(R.layout.server_row, serversTL, false);
                final boolean locked = server.optBoolean("passworded", false);

                ((TextView) row.findViewById(R.id.nameTV)).setText("["
                        + server.optString("eldewritoVersion", "UNKNOWN") + "] "
                        + (locked ? "[\uD83D\uDD12] " : "") + server.optString("name")
                        + " (" + server.optString("hostPlayer") + ")");

                ((TextView) row.findViewById(R.id.gameTypeTV)).setText(server.optString("variant"));

                ((TextView) row.findViewById(R.id.mapTV)).setText(server.optString("map")
                        + " (" + server.optString("mapFile") + ")");

                int numPlayers = server.optInt("numPlayers", 0), maxPlayers = server.optInt("maxPlayers", 16);
                playersCount += numPlayers;

                ((TextView) row.findViewById(R.id.playerCountTV)).setText(numPlayers + "/" + maxPlayers);

                row.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!locked) {
                            returnServer(address);
                        } else {
                            final MaterialDialog dialog = new MaterialDialog.Builder(ServerBrowserActivity.this)
                                    .title("Enter Server Password")
                                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)
                                    .input("", "", false, new MaterialDialog.InputCallback() {
                                        @Override
                                        public void onInput(MaterialDialog materialDialog, CharSequence cs) {
                                            returnServer(address, cs.toString());
                                        }
                                    })
                                    .positiveText(R.string.action_connect)
                                    .negativeText(R.string.action_cancel)
                                    .build();
                            if (dialog.getInputEditText() != null) {
                                dialog.getInputEditText().setOnKeyListener(new View.OnKeyListener() {
                                    @Override
                                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                                            dialog.getActionButton(DialogAction.POSITIVE).performClick();
                                            return true;
                                        }
                                        return false;
                                    }
                                });
                            }
                            dialog.show();
                        }
                    }
                });

                serversTL.addView(row);
                ++serverCount;
                inflater.inflate(R.layout.row_separator, serversTL, true);

                toolbarCenterTV.setText(playersCount + " Player" + (playersCount != 1 ? "s" : "")
                        + " On " + serverCount + " Server" + (serverCount != 1 ? "s" : ""));
            }
        });
    }

    private void updateServerList(final int ms) {
        if (ms < 0 || ms >= MASTER_SERVERS.length) return;
        emptyServerList();
        JSON.getObjectAsync(MASTER_SERVERS[ms], new JSON.ObjectCallback() {
            @Override
            public void done(JSONObject result) {
                if (result != null) {
                    currentMS = ms;
                    if (result.optInt("listVersion", 1) == 1) {
                        JSONObject res = result.optJSONObject("result");
                        if (res != null) {
                            if (res.optInt("code", 0) == 0) {
                                JSONArray servers = res.optJSONArray("servers");
                                if (servers != null) {
                                    for (int i = 0; i < servers.length(); ++i) {
                                        final String address = servers.optString(i);
                                        JSON.getObjectAsync("http://" + address, new JSON.ObjectCallback() {
                                            @Override
                                            public void done(JSONObject result) {
                                                addServer(address, result);
                                            }
                                        });
                                    }
                                }
                            } else toast("Error (" + res.optInt("code") + "): "
                                    + res.optString("msg"), Toast.LENGTH_SHORT);
                        }
                    }
                } else {
                    updateServerListWithNext(ms);
                }
            }
        });
    }

    private void updateServerList() {
        updateServerList(startMS = currentMS);
    }

    private void updateServerListWithNext(int ms) {
        int next = (ms + 1) % MASTER_SERVERS.length;
        if (next != startMS) updateServerList(next);
        else toast("Unable to update server list!", Toast.LENGTH_SHORT);
    }

    private void toast(final CharSequence text, final int duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ServerBrowserActivity.this, text, duration).show();
            }
        });
    }
}
